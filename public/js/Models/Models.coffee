window.Album = Backbone.Model.extend {
  isFirstTrack: (index) ->
    index == 0

  isLastTrack: (index) ->
    @get('tracks').length - 1 == index

  trackUrlAtIndex: (index) ->
    if @get('tracks').length >= index
      @get('tracks')[index].url
    else
      null
}

window.Player = Backbone.Model.extend {
  defaults: {
    'currentAlbumIndex': 0
    'currentTrackIndex': 0
    'state': 'stop'
  }

  initialize: ->
    @playlist = new Playlist()

  play: ->
    @set { 'state': 'play' }

  pause: ->
    @set { 'state': 'pause' }

  reset: ->
    @set { 'currentAlbumIndex': 0 }
    @set { 'currentTrackIndex': 0 }
    @set { 'state': 'stop' }
    @logCurrentAlbumAndTrack()

  isPlaying: ->
    'play' == @get 'state'

  isStopped: ->
    !@isPlaying()

  currentAlbum: ->
    @playlist.at @get 'currentAlbumIndex'

  currentTrack: ->
    currentTrack = @currentAlbum()
    if currentTrack
      currentTrack.get('tracks')[@get 'currentTrackIndex']
    else
      null

  currentTrackUrl: ->
    album = @currentAlbum()
    if album
      album.trackUrlAtIndex @get 'currentTrackIndex'
    else
      null

  nextTrack: ->
    currentTrackIndex = @get 'currentTrackIndex'
    currentAlbumIndex = @get 'currentAlbumIndex'

    if @currentAlbum()
      if @playlist.isLastAlbum(currentAlbumIndex) && @currentAlbum().isLastTrack(currentTrackIndex)
        @set { 'currentAlbumIndex': 0 }
        @set { 'currentTrackIndex': 0 }
      else if !@playlist.isLastAlbum(currentAlbumIndex) && @currentAlbum().isLastTrack(currentTrackIndex)
        @set { 'currentAlbumIndex': currentAlbumIndex + 1 }
        @set { 'currentTrackIndex': 0 }
      else
        @set { 'currentTrackIndex': currentTrackIndex + 1 }

      @logCurrentAlbumAndTrack()

  prevTrack: ->
    currentTrackIndex = @get 'currentTrackIndex'
    currentAlbumIndex = @get 'currentAlbumIndex'

    if @currentAlbum()
      if @playlist.isFirstAlbum(currentAlbumIndex) && @currentAlbum().isFirstTrack(currentTrackIndex)
        @set { 'currentAlbumIndex': @playlist.models.length - 1 }
        @set { 'currentTrackIndex': @currentAlbum().get('tracks').length - 1  }
      else if !@playlist.isFirstAlbum(currentAlbumIndex) && @currentAlbum().isFirstTrack(currentTrackIndex)
        @set { 'currentAlbumIndex': currentAlbumIndex - 1 }
        @set { 'currentTrackIndex': @currentAlbum().get('tracks').length - 1  }
      else
        @set { 'currentTrackIndex': currentTrackIndex - 1 }

      @logCurrentAlbumAndTrack()

  currentAlbumAndTrack: ->
    currentTrack = @currentTrack()
    currentAlbum = @currentAlbum()
    if currentAlbum && currentTrack
      @get('currentTrackIndex') + 1 + ". " + currentTrack['title'] + " from " + currentAlbum.get('title')
    else
      null

  logCurrentAlbumAndTrack: ->
    console.log @currentAlbumAndTrack()
}

