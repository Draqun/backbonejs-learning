$ ->
  window.AlbumView = Backbone.View.extend {
    template: _.template ($ '#album-template').html()
    tagName: "li"
    className: "album"

    initialize: ->
      _.bindAll @, 'render'

    render: ->
      ($ @el).html @template @model.toJSON()
      @
  }

  window.PlaylistAlbumView = AlbumView.extend {
    events: {
      'click .queue.remove': 'removeFromPlaylist'
    }

    initialize: ->
      _.bindAll @, 'render', 'remove', 'updateState', 'updateTrack'
      @model.bind 'remove', @remove
      @options.player.bind 'change:state', @updateState
      @options.player.bind 'change:currentTrackIndex', @updateTrack
      @

    render: ->
      ($ @el).html @template @model.toJSON()
      @updateTrack()
      @

    removeFromPlaylist: ->
      @options.player.playlist.remove @model
      @options.player.reset()

    updateState: ->
      isAlbumCurrent = @options.player.currentAlbum() == @model
      ($ @el).toggleClass 'current', isAlbumCurrent
      @

    updateTrack: ->
      isAlbumCurrent = @options.player.currentAlbum() == @model
      if isAlbumCurrent
        currentTrackIndex = @options.player.get 'currentTrackIndex'
        (@$ 'li').each (index, el) ->
          ($ el).toggleClass 'current', index == currentTrackIndex
      @updateState()
  }

  window.PlaylistView = Backbone.View.extend {
    tagName: 'section'
    className: 'playlist'

    events: {
      'click .play': 'play'
      'click .pause': 'pause'
      'click .next': 'nextTrack'
      'click .prev': 'prevTrack'
    }

    initialize: ->
      _.bindAll @, 'render', 'renderAlbum', 'queueAlbum', 'updateState', 'updateTrack'
      @template = _.template ($ '#playlist-template').html()
      @collection.bind 'reset', @render
      @collection.bind 'add', @renderAlbum

      @player = @options.player
      @player.bind 'change:state', @updateState
      @player.bind 'change:currentTrackIndex', @updateTrack
      @createAudio()


      @library = @options.library
      @library.bind 'select', @queueAlbum
      @

    render: ->
      ($ @el).html @template @player.toJSON()

      (@$ 'button.pause').toggle @player.isPlaying()
      (@$ 'button.play').toggle @player.isStopped()
      @

    createAudio: ->
      @audio = new Audio()

    queueAlbum: (album) ->
      @collection.add(album)

    renderAlbum: (album) ->
      view = new PlaylistAlbumView {
        model: album
        player: @player
        playlist: @playlist
      }
      (@$ 'ul').append view.render().el
      @

    updateState: ->
      @updateTrack()
      (@$ 'button.play').toggle @player.isStopped()
      (@$ 'button.pause').toggle @player.isPlaying()

    updateTrack: ->
      currentTrackUrl = @player.currentTrackUrl()
      @audio.src = currentTrackUrl
      if (@player.get 'state') == 'play'
        @audio.play()
      else
        @audio.pause()

    play: ->
      @player.play()

    pause: ->
      @player.pause()

    nextTrack: ->
      @player.nextTrack()

    prevTrack: ->
      @player.prevTrack()
  }

  window.LibraryAlbumView = AlbumView.extend {
    events: {
      'click .queue.add': 'select'
    }

    select: ->
      @collection.trigger 'select', @model
      
  }

  window.LibraryView = Backbone.View.extend {
    tagName: 'section'
    className: 'library'

    initialize: ->
      _.bindAll @, 'render'
      @template = _.template ($ '#library-template').html()
      @collection.bind 'reset', @render
      @

    render: ->
      collection = @collection

      ($ @el).html @template {}
      $albums = @$ '.albums'
      collection.each (album) ->
        view = new LibraryAlbumView {
          model: album
          collection: collection
        }
        $albums.append view.render().el
      @
  }

