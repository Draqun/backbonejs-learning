window.Albums = Backbone.Collection.extend {
  model: Album,
  url: '/albums'
}

window.Playlist = Albums.extend {
  isFirstAlbum: (index) ->
    index == 0

  isLastAlbum: (index) ->
    index == @models.length - 1
}

window.library = new Albums()
window.player = new Player()
