window.BackboneTunes = Backbone.Router.extend {
  routes: {
    '': 'home'
    'home': 'home'
    'blank': 'blank'
  }

  initialize: ->
    @playlistView = new PlaylistView {
      collection: window.player.playlist
      player: window.player
      library: window.library
    }

    @libraryView = new LibraryView {
      collection: window.library
    }
    @

  home: ->
    $container = ($ '#container')
    $container.empty()
    $container.append @playlistView.render().el
    $container.append @libraryView.render().el
}

$ ->
  window.App = new BackboneTunes();
  Backbone.history.start()

