albumData = [{
    "title":  "Album A",
    "artist": "Artist A",
    "tracks": [
        {
            "title": "Track A",
            "url": "/music/Album A Track A.mp3"
        },
        {
            "title": "Track B",
            "url": "/music/Album A Track B.mp3"
        }]
}, {
    "title": "Album B",
    "artist": "Artist B",
    "tracks": [
        {
            "title": "Track A",
            "url": "/music/Album B Track A.mp3"
        },
        {
            "title": "Track B",
            "url": "/music/Album B Track B.mp3"
    }]
}];

describe "Album", ->
  beforeEach ->
    @album = new Album(albumData[0])

  it "creates from data", ->
    (expect @album.get('tracks').length).toEqual 2

  describe "first track", ->
    it "identifies correct first track", ->
      (expect @album.isFirstTrack(0)).toBeTruthy

  describe "last track", ->
    it "identifies correct last track", ->
      (expect @album.isLastTrack(0)).toBeTruthy

  describe "track url", ->
    it "returns url to track", ->
      (expect @album.trackUrlAtIndex(1)).toEqual "/music/Album A Track B.mp3"

describe "Playlist", ->
  beforeEach ->
    @playlist = new Playlist()
    @playlist.add albumData[0]

  it "identifies first album as first", ->
    (expect @playlist.isFirstAlbum(0)).toBeTruthy()

  it "rejects non-first album as first", ->
    (expect @playlist.isFirstAlbum(1)).toBeFalsy()

  it "identifies last album as first", ->
    @playlist.add albumData[1]
    (expect @playlist.isLastAlbum(1)).toBeTruthy()

  it "rejects non-last album as first", ->
    @playlist.add albumData[1]
    (expect @playlist.isLastAlbum(0)).toBeFalsy()

